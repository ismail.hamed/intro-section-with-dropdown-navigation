const navToggle = document.querySelector(".nav-toggle");
navToggle.addEventListener("click", () => {
  document.body.classList.toggle("nav-open");
  var backgroundColor = document.body.style.backgroundColor;
  if (backgroundColor === "") {
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
  } else {
    document.body.style.backgroundColor = "";
  }
});

var acc = document.getElementsByClassName("nav__link");
var i;
for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
window.addEventListener("resize", () => {
  document.body.classList.remove("nav-open");
  document.body.style.backgroundColor = "";
  var elems = document.querySelectorAll(".active");
  [].forEach.call(elems, function (el) {
    el.classList.remove("active");
    var panel = el.nextElementSibling;
    panel.style.display = "none";
  });
});
